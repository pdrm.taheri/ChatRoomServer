
#include <QtWidgets/QApplication>
#include "Controller/Server.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    Server::run();

    return app.exec();
}