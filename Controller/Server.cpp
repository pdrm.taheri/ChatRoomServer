//
// Created by pedram on 3/14/17.
//

#include "Server.h"

// Definition of static variables
static Server *instance;

Server::Server()
{
    ConnectionHandler::getInstance()->setOnConnectionClosedHandler(Server::onClientDisconnected);
    ConnectionHandler::getInstance()->setOnNewConnectionHandler(Server::onNewClientConnect);
    ConnectionHandler::getInstance()->setOnNewMessageHandler(Server::onNewMessage);
}

Server::~Server()
{
    delete instance;
}

void Server::onNewClientConnect(Connection *newConnection)
{
    qDebug() << "[SERVER] New connection!";
    newConnection->setId(ConnectionDataStore::getInstance()->getConnections().size());
    ConnectionDataStore::getInstance()->getConnections().emplace_back(std::move(newConnection));
    UsersDataStore::getInstance()->getUsers().emplace_back(new User(newConnection->getId(), "AsgharAgha"));
}

void Server::onNewMessage(int senderId, QByteArray message)
{
    std::string str = "[SERVER] New message: {" + message.toStdString() + "} from " +
                      UsersDataStore::getInstance()->getUsers()[senderId]->getName() + " With ID: ";
    qDebug() << str.c_str() << UsersDataStore::getInstance()->getUsers()[senderId]->getId() << endl;

    for (Connection *connection : ConnectionDataStore::getInstance()->getConnections())
    {
        if (connection->getId() == senderId)
        {
            continue;
        }

        connection->getSocket()->write(message);
    }
}

void Server::onClientDisconnected(Connection *connection)
{
    std::vector<Connection *>::iterator iter = std::find(ConnectionDataStore::getInstance()->getConnections().begin(),
                                                         ConnectionDataStore::getInstance()->getConnections().end(),
                                                         connection);

    qDebug() << "Client about disconnect with ID: " << (*iter)->getId();

    auto indexOf = iter - ConnectionDataStore::getInstance()->getConnections().begin();
    ConnectionDataStore::getInstance()->getConnections().erase(iter);

    UsersDataStore::getInstance()->getUsers().erase(UsersDataStore::getInstance()->getUsers().begin() + indexOf);
    delete connection;

    updateIdsAfterDisconnection();
}

void Server::updateIdsAfterDisconnection()
{
    for (int i = (int) ConnectionDataStore::getInstance()->getConnections().size() - 1; i >= 0; --i)
    {
        if (ConnectionDataStore::getInstance()->getConnections()[i]->getId() != i)
        {
            ConnectionDataStore::getInstance()->getConnections()[i]->setId(i);
            UsersDataStore::getInstance()->getUsers()[i]->setId(i);
        }
        else
        {
            break;
        }
    }
}

Server *Server::getInstance()
{
    if (instance == nullptr)
    {
        instance = new Server();
    }

    return instance;
}

void Server::run()
{
    getInstance(); // To initialize handlers

    QHostAddress address(HOST_ADDRESS);
    ConnectionHandler::getInstance()->listenOn(address, HOST_PORT);
}

