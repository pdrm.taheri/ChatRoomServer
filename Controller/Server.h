//
// Created by pedram on 3/14/17.
//

#ifndef CHATROOMSERVER_SERVER_H
#define CHATROOMSERVER_SERVER_H


#include <QtNetwork/QTcpServer>
#include <QtNetwork/QHostAddress>
#include "../Model/ConnectionDataStore.h"
#include "Connection/ConnectionHandler.h"
#include "../Model/UsersDataStore.h"
#include "Connection/Connection.h"
#include "../Model/User.h"
#include "../Constants.h"

class Server
{
public:
    static void onNewMessage(int senderId, QByteArray message);
    static void onNewClientConnect(Connection *newConnection);
    static void onClientDisconnected(Connection *connection);

    static Server *getInstance();
    static void run();

private:
    Server();
    ~Server();

    static void updateIdsAfterDisconnection();
};


#endif //CHATROOMSERVER_SERVER_H
