//
// Created by pedram on 3/14/17.
//

#ifndef CHATROOMSERVER_CONNECTION_H
#define CHATROOMSERVER_CONNECTION_H


#include <QtNetwork/QTcpSocket>

class Connection : public QObject
{
public:
    Connection(QTcpSocket *socket);
    ~Connection();

    QTcpSocket *getSocket();
    int getId() const;
    void setId(int id);

    void setOnConnectionClosedHandler(void (*handler)(Connection *connection));
    void setOnNewBytesToReadHandler(void (*handler)(Connection *connection));

private:
    QTcpSocket *socket;
    int id;
    void onNewBytesToRead();
    void onConnectionClosed();

    void (*onNewBytesToReadHandler)(Connection *connection);
    void (*onConnectionClosedHandler)(Connection *connection);
};


#endif //CHATROOMSERVER_CONNECTION_H
