//
// Created by pedram on 3/14/17.
//

#ifndef CHATROOMSERVER_CONNECTIONHANDLER_H
#define CHATROOMSERVER_CONNECTIONHANDLER_H


#include <QtNetwork/QTcpServer>
#include "Connection.h"

class ConnectionHandler : public QObject
{
public:
    static ConnectionHandler *getInstance();

    // Events and handlers
    static void onNewConnection();
    static void onNewWriteOnSocket(Connection *socket);
    static void onSocketClosed(Connection *socket);

    void setOnNewMessageHandler(void (*handler)(int senderId, QByteArray message));
    void setOnNewConnectionHandler(void (*handler)(Connection *connection));
    void setOnConnectionClosedHandler(void (*handler)(Connection *connection));

    void listenOn(QHostAddress &hostAddress, unsigned short port);

private:
    ConnectionHandler();
    ~ConnectionHandler();

    void (*onNewMessageHandler)(int senderId, QByteArray message);
    void (*onNewConnectionHandler)(Connection *connection);
    void (*onConnectionClosedHandler)(Connection *connection);

    QTcpServer *tcpServer;

};


#endif //CHATROOMSERVER_CONNECTIONHANDLER_H
