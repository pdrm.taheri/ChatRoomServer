//
// Created by pedram on 3/14/17.
//

#include "User.h"

User::User(int id, std::string name) : id(id), name(name)
{
}

User::~User()
{
}

int User::getId() const
{
    return id;
}

std::string User::getName() const
{
    return name;
}

void User::setId(int id)
{
    this->id = id;
}
