//
// Created by pedram on 3/14/17.
//

#include "UsersDataStore.h"

// Definition of static variables
static UsersDataStore *instance;

UsersDataStore *UsersDataStore::getInstance()
{
    if (instance == nullptr)
    {
        instance = new UsersDataStore();
    }
    return instance;
}

std::vector<User *> &UsersDataStore::getUsers()
{
    return activeUsers;
}

UsersDataStore::UsersDataStore()
{
}

UsersDataStore::~UsersDataStore()
{
    delete instance;
}
