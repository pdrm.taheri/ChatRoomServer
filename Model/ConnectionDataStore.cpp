//
// Created by pedram on 3/14/17.
//

#include "ConnectionDataStore.h"

// Definition of static variables
static ConnectionDataStore *instance;

ConnectionDataStore *ConnectionDataStore::getInstance()
{
    if (instance == nullptr)
    {
        instance = new ConnectionDataStore();
    }

    return instance;
}

std::vector<Connection *> &ConnectionDataStore::getConnections()
{
    return openConnections;
}

ConnectionDataStore::ConnectionDataStore()
{
}

ConnectionDataStore::~ConnectionDataStore()
{
    delete instance;
}

