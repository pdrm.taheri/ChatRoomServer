//
// Created by pedram on 3/14/17.
//

#ifndef CHATROOMSERVER_CONNECTIONDATASTORE_H
#define CHATROOMSERVER_CONNECTIONDATASTORE_H


#include <vector>
#include <QtNetwork/QTcpSocket>
#include "../Controller/Connection/Connection.h"

class ConnectionDataStore
{
public:
    static ConnectionDataStore *getInstance();
    std::vector<Connection *> &getConnections();

private:
    ConnectionDataStore();
    ~ConnectionDataStore();

    std::vector<Connection *> openConnections;
};


#endif //CHATROOMSERVER_CONNECTIONDATASTORE_H
