//
// Created by pedram on 3/14/17.
//

#ifndef CHATROOMSERVER_USER_H
#define CHATROOMSERVER_USER_H


#include <QtNetwork/QTcpSocket>

class User
{
public:
    User(int id, std::string name);
    ~User();

    int getId() const;
    std::string getName() const;

    void setId(int id);

private:
    const std::string name;
    int id;

};


#endif //CHATROOMSERVER_USER_H
