//
// Created by pedram on 3/14/17.
//

#ifndef CHATROOMSERVER_USERSDATASTORE_H
#define CHATROOMSERVER_USERSDATASTORE_H


#include "User.h"

class UsersDataStore
{
public:
    static UsersDataStore *getInstance();
    std::vector<User *> &getUsers();

private:
    UsersDataStore();
    ~UsersDataStore();

    std::vector<User *> activeUsers;
};


#endif //CHATROOMSERVER_USERSDATASTORE_H
